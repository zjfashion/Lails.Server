﻿using Autofac;
using Autofac.Integration.WebApi;
using Lails.Server;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(AutofacConfig), "Initialize")]
namespace Lails.Server
{
    /// <summary>
    /// 
    /// </summary>
    public static class AutofacConfig
    {
        static ContainerBuilder builder = new ContainerBuilder();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Initialize(HttpConfiguration config)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(RegisterServices(builder));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetEntryAssembly());
            //builder.Register(c => new CacheService()).SingleInstance();
            //foreach (var instance in instances)
            //{
            //    //builder.Register(c => instance).SingleInstance();
            //    builder.RegisterType(instance).SingleInstance();
            //}
            //builder.Register((c) => new Dispatcher((CacheService)c.ResolveOptional(typeof(CacheService)))).SingleInstance();
            return builder.Build();
        }
        
        public static ContainerBuilder Builder
        {
            get { return builder; }
        }
    }
}