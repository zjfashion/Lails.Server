﻿
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Lails.Server
{
    internal class AppService : ServiceControl
    {
        private Action _start, _stop;

        public AppService(Action start, Action stop)
        {
            _start = start;
            _stop = stop;
        }

        public bool Start(HostControl hostControl)
        {
            AppLogger.Info("服务已启动！");
            //主逻辑服务
            try
            {
                _start?.Invoke();
            }
            catch (Exception ex)
            {
                AppLogger.Info($"未处理的主进程异常: {ex.Message}");
                AppLogger.Info(ex.StackTrace);
            }
            //WebAPI服务
            if (AppConfig.Default.HttpPort > 0)
            {
                WebApp.Start<WebAPIConfig>($"http://*:{AppConfig.Default.HttpPort}");
                AppLogger.Info($"WebAPI启动，端口：{AppConfig.Default.HttpPort}");
            }
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _stop?.Invoke();
            return true;
        }
    }
}
