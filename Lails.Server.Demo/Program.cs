﻿using Autofac;
using Lails.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.Server.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            AppServer.Run(AppConfig.Default, () =>
            {
                Config config = AppConfig.Load<Config>();
                AppLogger.Info("Server Start.");

                System.Timers.Timer timer = new System.Timers.Timer(1000 * 10);
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
                //System.Threading.ThreadPool.QueueUserWorkItem(t =>
                //{
                //    System.Threading.Thread.Sleep(1000);
                //    try
                //    {
                //        throw new Exception("测试异常信息");
                //    }catch(Exception ex)
                //    {
                //        AppLogger.Info($"已处理的异常：{ex.Message}");
                //    }
                //});

                //注入
                AutofacConfig.Builder.RegisterType(typeof(Logger)).SingleInstance();
                //AutofacConfig.Builder.Register((c) => new Dispatcher((CacheService)c.ResolveOptional(typeof(CacheService)))).SingleInstance();

            }, () => {
                AppLogger.Info("Server Stop.");
            });
        }

        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            try
            {
                throw new Exception("测试异常信息");
            }
            catch (Exception ex)
            {
                AppLogger.Info($"已处理的异常：{ex.Message}");
            }
        }
    }
}
